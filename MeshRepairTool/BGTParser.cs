﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Xml;
using Npgsql;
using NetTopologySuite.Geometries;
using NetTopologySuite.Geometries.Utilities;
using NetTopologySuite.Index.Strtree;
using System.Windows.Forms;
using System.Drawing;

public enum MatchState
{
    NoMatch,
    Direct,
    VertexVertex,
    EdgeVertex,
    VertexEdge,
    EdgeEdge,
}

partial class BGTParser
{
    private const float EPSILON = 0.05f;
    private const double SPHERE_RADIUS = 0.024f;

    // Add any other BGT GML files that you want to load here.
    public string[] bgtObjectTypes = new string[] { "wegdeel", "pand", "overbruggingsdeel", "tunneldeel", "onbegroeidterreindeel", "begroeidterreindeel", "waterdeel", "ondersteunendwegdeel", "scheiding", "ondersteunendwaterdeel", "kunstwerkdeel", "paal", "vegetatieobject" };

    // Object types that should be checked for connections
    private string[] connectedObjectTypes = new string[] { "wegdeel", "onbegroeidterreindeel", "begroeidterreindeel", "ondersteunendwaterdeel", "pand", "ondersteunendwegdeel", "kunstwerkdeel", "tunneldeel", "vegetatieobject", "scheiding" };

    // Object types that should be repaired
    // Uncertain about tunnelpart and waterdeel
    private string[] reparableObjectTypes = new string[] { "wegdeel", "onbegroeidterreindeel", "begroeidterreindeel", "ondersteunendwaterdeel", "pand", "ondersteunendwegdeel", "kunstwerkdeel", /*"tunneldeel",*/ "vegetatieobject", /*"waterdeel", */"scheiding" };


    private string fileLocation = @"E:\Desktop\utrecht2";
    //private string fileLocation = @"C:\Users\dimas\Desktop\Utrecht\bgt";        // Hardcoded, pls ignore
    //private string fileLocation = @"C:\Users\dimas\Desktop\BGTNL";        // Hardcoded, pls ignore

    public PictureBox pictureBox;
    public bool continuous;

    private Geometry drawingInnerPolygon;
    private List<Geometry> drawingInnerObjects;
    private List<Geometry> drawingOuterObjects;

    DrawnObjects drawer = new DrawnObjects();


    #region Edge Finding

    public List<Connection> FindConnectionsNew(Envelope window = null)
    {
        List<Connection> connections = new List<Connection>();
        Dictionary<int, List<SimpleBGTObject>> layerDict = new Dictionary<int, List<SimpleBGTObject>>();
        Dictionary<int, STRtree<SimpleBGTObject>> treeDict = new Dictionary<int, STRtree<SimpleBGTObject>>();

        var conn = DBHelper.GetPostgresConnection();

        if (window == null)
        {
            window = new Envelope(123500, 125000, 480500, 482000);
        }

        var allObjects = DBHelper.GetAllPostGIS(conn, window, true);

        for (int i = -5; i < 5; i++)
        {
            //layerDict[i] = DBHelper.GetObjectsAtHeight(i, conn).Where(x => connectedObjectTypes.Contains(x.type)).ToList();

            layerDict[i] = allObjects.Where(x => x.relativeHeight == i && connectedObjectTypes.Contains(x.type)).ToList();
            var tree = new STRtree<SimpleBGTObject>();
            foreach (var obj in layerDict[i])
                tree.Insert(obj.geometry.EnvelopeInternal, obj);
            tree.Build();
            treeDict[i] = tree;
        }

        for (int i = -5; i < 0; i++)
        {
            connections.AddRange(FindConnectionsBetweenLayers(layerDict[i], treeDict[i + 1]));
        }

        for (int i = 4; i > 0; i--)
        {
            connections.AddRange(FindConnectionsBetweenLayers(layerDict[i], treeDict[i - 1]));
        }

        DBHelper.UpsertConnections(connections);

        return connections;
    }

    public List<Connection> FindConnectionsBetweenLayers(List<SimpleBGTObject> left, STRtree<SimpleBGTObject> right)
    {
        List<Connection> connections = new List<Connection>();

        foreach (var current in left)
        {
            Envelope env = new Envelope(current.geometry.EnvelopeInternal);
            env.ExpandBy(0.1f);
            var nearby = right.Query(env);

            foreach (var next in nearby)
            {
                if (current.ID == "b8e46e0a0-d665-11e7-8ec4-89be260623ee" && next.ID == "b8e26d5b3-d665-11e7-8ec4-89be260623ee")
                {
                    Console.WriteLine("This one");
                }

                var isValidOp = new NetTopologySuite.Operation.Valid.IsValidOp(next.geometry);
                if (!isValidOp.IsValid)
                {
                    Console.WriteLine("Error at object " + next.ID + " of type " + next.type);
                    Console.WriteLine("feature.Geometry is not valid:" + isValidOp.ValidationError.Message);
                    switch (isValidOp.ValidationError.ErrorType)
                    {
                        // handle error types.
                    }
                    //continue;
                }

                // Find common edges between current and next
                if (current.relativeHeight < next.relativeHeight)
                    connections.AddRange(GetCommonEdges(current, next));
                else connections.AddRange(GetCommonEdges(next, current));
            }
        }

        return connections;
    }

    public List<Connection> FindConnectionsBetweenLayers(List<SimpleBGTObject> left, List<SimpleBGTObject> right)
    {
        List<Connection> connections = new List<Connection>();

        foreach (var current in left)
        {
            

            foreach (var next in right)
            {
                if (current.ID == "b8e46e0a0-d665-11e7-8ec4-89be260623ee" && next.ID == "b8e26d5b3-d665-11e7-8ec4-89be260623ee")
                {
                    Console.WriteLine("This one");
                }
                
                var isValidOp = new NetTopologySuite.Operation.Valid.IsValidOp(next.geometry);
                if (!isValidOp.IsValid)
                {
                    Console.WriteLine("Error at object " + next.ID + " of type " + next.type);
                    Console.WriteLine("feature.Geometry is not valid:" + isValidOp.ValidationError.Message);
                    switch (isValidOp.ValidationError.ErrorType)
                    {
                        // handle error types.
                    }
                    //continue;
                }

                // Some distance based early outs
                double dist = current.geometry.EnvelopeInternal.Distance(next.geometry.EnvelopeInternal);
                //if (current.geometry.EnvelopeInternal.Distance(next.geometry.EnvelopeInternal) > 0.1) continue;
                if (dist > 0.1) continue;
                if (current.geometry.Distance(next.geometry) > 0.1) continue;

                //if (current.geometry.Touches(next.geometry))
                //{
                    // Find common edges between current and next
                    connections.AddRange(GetCommonEdges(current, next));
                //}
            }
        }

        return connections;
    }

    // TODO fix this so it actually returns stuff
    public List<Connection> GetCommonEdges(SimpleBGTObject a, SimpleBGTObject b)
    {
        List<Connection> commonEdges = new List<Connection>();

        List<Polygon> polygonsA = PolygonExtracter.GetPolygons(a.geometry).Select(x => (Polygon)x).ToList();
        List<Polygon> polygonsB = PolygonExtracter.GetPolygons(b.geometry).Select(x => (Polygon)x).ToList();

        foreach (var polyA in polygonsA)
        {
            foreach (var polyB in polygonsB)
            {
                //if (polyA.Touches(polyB))   // TODO: This gives false negatives
                {
                    var outlineA = polyA.Boundary.Coordinates;
                    var outlineB = polyB.Boundary.Coordinates;

                    for (int indexA = 0; indexA < outlineA.Length; indexA++)
                    {
                        Edge edgeA = GeometryHelper.GetEdge(outlineA, indexA);
                        if (edgeA.start.Equals(edgeA.end)) continue;

                        for (int indexB = 0; indexB < outlineB.Length; indexB++)
                        {
                            Edge edgeB = GeometryHelper.GetEdge(outlineB, indexB);

                            if (edgeA == edgeB)
                            {
                                if (edgeA.start.Equals(edgeB.start)) continue;

                                Connection c = new Connection();
                                c.a = a;
                                c.b = b;
                                c.indexA = indexA;
                                c.indexB = indexB;
                                c.geometry = new LineString(new Coordinate[] { edgeA.start, edgeA.end });
                                //Console.WriteLine("Connection found between {0} and {1} at {2} and {3}", c.a.ID, c.b.ID, indexA, indexB);
                                commonEdges.Add(c);
                            }
                        }
                    }
                }
            }
        }

        return commonEdges;
    }
    #endregion

    public void TestContains()
    {
        var coords = new Coordinate[] { new Coordinate(0, 0), new Coordinate(0, 1), new Coordinate(1, 1), new Coordinate(1, 0), new Coordinate(0, 0) };
        LinearRing ring = new LinearRing(coords);
        Polygon p = new Polygon(ring);

        NetTopologySuite.Geometries.Point testPoint = new NetTopologySuite.Geometries.Point(coords[0]);
        LineString edge = new LineString(new Coordinate[] { coords[0], coords[1] });
        LineString reverse = new LineString(new Coordinate[] { coords[1], coords[0] });
        LineString touchedNotCovered = new LineString(new Coordinate[] { coords[0], new Coordinate(0, -1) });

        bool containsPoint = p.Contains(testPoint);
        bool containsEdge = p.Contains(edge);
        bool containsNotCovered = p.Contains(touchedNotCovered);

        bool touchesPoint = p.Touches(testPoint);
        bool touchesEdge = p.Touches(edge);
        bool touchesReverse = p.Touches(reverse);
        bool touchesNotCovered = p.Touches(touchedNotCovered);


        bool intersectsPoint = p.Intersects(testPoint);
        bool intersectsEdge = p.Intersects(edge);


        bool coversPoint = p.Covers(testPoint);
        bool coversEdge = p.Covers(edge);
        bool coversReverse = p.Covers(reverse);
        bool coversNotCovered = p.Covers(touchedNotCovered);


        Console.WriteLine("Polygon Contains point: {0}", containsPoint);
        Console.WriteLine("Polygon Contains edge: {0}", containsEdge);
        Console.WriteLine("Polygon Contains not covered {0}", containsNotCovered);

        Console.WriteLine("Polygon Touches point: {0}", touchesPoint);
        Console.WriteLine("Polygon Touches edge: {0}", touchesEdge);
        Console.WriteLine("Polygon Touches reverse edge: {0}", touchesReverse);
        Console.WriteLine("Polygon Touches not covered: {0}", touchesNotCovered);

        Console.WriteLine("Polygon Intersects point: {0}", intersectsPoint);
        Console.WriteLine("Polygon Intersects edge: {0}", containsEdge);

        Console.WriteLine("Polygon Covers point: {0}", coversPoint);
        Console.WriteLine("Polygon Covers edge: {0}", coversEdge);
        Console.WriteLine("Polygon Covers reverse edge: {0}", coversReverse);
        Console.WriteLine("Polygon Covers notCovered: {0}", coversNotCovered);

        Geometry g = GeometryHelper.AddBetween(p, coords[0], coords[1], new Coordinate(0, 0.5));
        Console.WriteLine(g.Coordinates.Length);
    }

    public int Mod(int k, int n) { return (k % n + n) % n; }

    public void Paint(PaintEventArgs e)
    {
        if (drawer.p == null)
            drawer.p = pictureBox;

        drawer.Draw(e);
    }

    private STRtree<SimpleBGTObject> BuildSTRTree(List<SimpleBGTObject> objects)
    {
        STRtree<SimpleBGTObject> tree = new STRtree<SimpleBGTObject>();

        foreach(var o in objects)
        {
            tree.Insert(o.geometry.EnvelopeInternal, o);
        }

        tree.Build();

        return tree;
    }
}

// Struct to store a reference to particular vertex within
public struct VertexMap
{
    public int objectIndex;
    public int subMeshIndex;
    public int vertexIndex;
}

public class Transformation
{
    public LineString e;
    public NetTopologySuite.Geometries.Point v;
    public int p;
    public string id;

    public Transformation(LineString e, NetTopologySuite.Geometries.Point v, int p, string id = "")
    {
        this.e = e;
        this.v = v;
        this.p = p;
        this.id = id;
    }
}