﻿using NetTopologySuite.Geometries;
using NetTopologySuite.Geometries.Utilities;
using NetTopologySuite.Index.Strtree;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

partial class BGTParser
{
    #region MeshRepair

    Stopwatch timer;
    bool DRAWER_ENABLED = true;

    public void RepairBetweenLayersSimple(Envelope window = null, bool findConnections = false, bool writeToDatabase = false)
    {
        var conn = DBHelper.GetPostgresConnection();

        if (window == null)
        {
            window = new Envelope(136250, 136750, 455200, 456200);
        }

        var allObjects = DBHelper.GetAllPostGIS(conn, window);

        Dictionary<int, Dictionary<string, SimpleBGTObject>> layerDict = new Dictionary<int, Dictionary<string, SimpleBGTObject>>();
        Dictionary<int, STRtree<SimpleBGTObject>> treeDict = new Dictionary<int, STRtree<SimpleBGTObject>>();

        long milliseconds = 0;
        timer = Stopwatch.StartNew();

        for (int i = -10; i < 10; i++)
        {
            layerDict[i] = allObjects
                .Where(x => x.relativeHeight == i && (x.geometry.GeometryType == "Polygon" || x.geometry.GeometryType == "MultiPolygon") /*&& reparableObjectTypes.Contains(x.type)*/)
                //.Where(x=> x.type == "wegdeel" || x.type == "scheiding")// TODO: Temp
                .ToDictionary(x => x.ID, x => x);

            var tree = new STRtree<SimpleBGTObject>();
            foreach (var obj in layerDict[i].Values)
                tree.Insert(obj.geometry.EnvelopeInternal, obj);
            tree.Build();
            treeDict[i] = tree;
        }

        milliseconds = timer.ElapsedMilliseconds;
        Console.WriteLine("Building trees: " + milliseconds);

        //Console.WriteLine("Heights: {0}, {1}", layerDict[0].Count, layerDict[1].Count);
        conn.Close();


        timer.Restart();

        List<int> layerIndices = new List<int>();
        for (int i = 1; i <= 5; i++)
        {
            layerIndices.Add(i);
            layerIndices.Add(-i);
        }

        foreach(int layer in layerIndices)
        {
            var clusters = NetTopologySuite.Operation.Union.UnaryUnionOp.Union(layerDict[layer]
                .Where(x => x.Value.type != "overbruggingsdeel" && x.Value.type != "pand")
                .Where(x => x.Value.geometry.GeometryType == "Polygon" || x.Value.geometry.GeometryType == "MultiPolygon")
                .Select(x => x.Value.geometry));

            foreach (Polygon cluster in PolygonExtracter.GetPolygons(clusters))
            {
                // To avoid large tunnels
                if (layer < 0 &&
                    (cluster.EnvelopeInternal.MaxX - cluster.EnvelopeInternal.MinX > 100
                    || cluster.EnvelopeInternal.MaxY - cluster.EnvelopeInternal.MinY > 100))
                    continue;

                // To avoid large structures at higher levels (should be repaired manually)
                if (layer > 0 &&
                    (cluster.EnvelopeInternal.MaxX - cluster.EnvelopeInternal.MinX > 100
                    || cluster.EnvelopeInternal.MaxY - cluster.EnvelopeInternal.MinY > 100))
                    continue;

                if (layer > 0)
                {
                    for (int i = layer - 1; i >= 0; i--)
                    {
                        FixTJoints(cluster, layerDict[layer], layerDict[i], treeDict[layer], treeDict[i]);
                    }
                }
                else if (layer < 0)
                {
                    for (int i = layer + 1; i <= 0; i++)
                    {
                        FixTJoints(cluster, layerDict[layer], layerDict[i], treeDict[layer], treeDict[i]);
                    }
                }
            }
        }

        milliseconds = timer.ElapsedMilliseconds;
        Console.WriteLine("Mesh repair: " + milliseconds);

        if (writeToDatabase)
        {
            foreach (var layer in layerDict)
            {
                DBHelper.UpsertAllPostGIS(layer.Value);
            }
        }

        if (findConnections)
        {
            List<Connection> connections = new List<Connection>();
            Dictionary<int, List<SimpleBGTObject>> layers = new Dictionary<int, List<SimpleBGTObject>>();
            Dictionary<int, STRtree<SimpleBGTObject>> connectionTreeDict = new Dictionary<int, STRtree<SimpleBGTObject>>();

            for (int i = -5; i < 5; i++)
            {
                layers[i] = layerDict[i].Values.Where(x => connectedObjectTypes.Contains(x.type) && (x.geometry.GeometryType == "Polygon" || x.geometry.GeometryType == "MultiPolygon")).ToList();

                var tree = new STRtree<SimpleBGTObject>();
                foreach (var obj in layerDict[i].Values)
                    tree.Insert(obj.geometry.EnvelopeInternal, obj);
                tree.Build();
                connectionTreeDict[i] = tree;
            }

            timer.Restart();

            for (int i = -5; i < 0; i++)
            {
                connections.AddRange(FindConnectionsBetweenLayers(layers[i], connectionTreeDict[i + 1]));
            }

            for (int i = 4; i > 0; i--)
            {
                connections.AddRange(FindConnectionsBetweenLayers(layers[i], connectionTreeDict[i - 1]));
            }

            milliseconds = timer.ElapsedMilliseconds;
            Console.WriteLine("Find connections: " + milliseconds);

            if (writeToDatabase)
            {
                DBHelper.UpsertConnections(connections);
            }
        }
    }

    public void FixTJoints(Polygon polygon, Dictionary<string, SimpleBGTObject> sameLayer, Dictionary<string, SimpleBGTObject> otherLayer, STRtree<SimpleBGTObject> sameLayerTree, STRtree<SimpleBGTObject> otherLayerTree, float epsilon = EPSILON)
    {
        List<SimpleBGTObject> innerObjects = new List<SimpleBGTObject>();   // sameLayer.Where(x => x.Value.geometry.Distance(polygon) < epsilon).Select(kvp => kvp.Value).ToList();
        List<SimpleBGTObject> outerObjects = new List<SimpleBGTObject>();   // otherLayer.Where(x => x.Value.geometry.Distance(polygon) < epsilon).Select(kvp => kvp.Value).ToList();
        Envelope env = new Envelope(polygon.EnvelopeInternal);
        env.ExpandBy(epsilon);

        var nearbySame = sameLayerTree.Query(env);
        foreach(var obj in nearbySame)
        {
            innerObjects.Add(sameLayer[obj.ID]);
        }

        var nearbyOther = otherLayerTree.Query(env);
        foreach(var obj in nearbyOther)
        {
            outerObjects.Add(otherLayer[obj.ID]);
        }

        if (DRAWER_ENABLED)
        {

            drawer.allInnerObjects = innerObjects;
            drawer.allOuterObjects = outerObjects;
            drawer.SetOffsets();
            drawer.CenterOnInner();

            pictureBox.Refresh();
        }

        // TODO: Process Vertices
        Console.ForegroundColor = ConsoleColor.Green;
        //Console.WriteLine("Fixing vertices");
        Console.ForegroundColor = ConsoleColor.White;

        for (int i = 0; i < polygon.Coordinates.Length; i++)
        {
            // TODO: It is possible for a new coordinate to already exist within the object.
            Coordinate newCoord = ProcessVertex(polygon.Coordinates[i], polygon, innerObjects, outerObjects);
            if (!newCoord.Equals(polygon.Coordinates[i]))
            {
                polygon = (Polygon)GeometryHelper.ReplaceCoordinate(polygon, polygon.Coordinates[i], newCoord);
                polygon.GeometryChanged();

                if (DRAWER_ENABLED)
                {
                    drawer.newVertex = newCoord;
                    pictureBox.Refresh();
                }
            }
        }

        // TODO: Process Vertices
        Console.ForegroundColor = ConsoleColor.Green;
        //Console.WriteLine("Fixing edges");
        Console.ForegroundColor = ConsoleColor.White;

        ProcessEdges(polygon, innerObjects, outerObjects, epsilon);

        // Write back to input lists
        foreach (var o in innerObjects)
        {
            if (sameLayer.ContainsKey(o.ID))
                sameLayer[o.ID].geometry = o.geometry;
            if (otherLayer.ContainsKey(o.ID))
                otherLayer[o.ID].geometry = o.geometry;
        }

        foreach (var o in outerObjects)
        {
            if (sameLayer.ContainsKey(o.ID))
                sameLayer[o.ID].geometry = o.geometry;
            if (otherLayer.ContainsKey(o.ID))
                otherLayer[o.ID].geometry = o.geometry;
        }
    }

    // TODO: Add heuristic so this will pick the best candidate rather than the closest (or direct) one, as that can be wrong.
    private Coordinate ProcessVertex(Coordinate innerVertex, Geometry geom, List<SimpleBGTObject> innerObjects, List<SimpleBGTObject> outerObjects, float epsilon = EPSILON)
    {
        bool debug = true;
        double minDist = double.MaxValue;
        MatchState matchState = MatchState.NoMatch;
        Coordinate matchingVertex = null;
        SimpleBGTObject matchingObject = null;
        string type = "";   // TODO this isn't used anymore

        foreach (var outerObj in outerObjects)
        {
            if (outerObj.ID == "b3fead445-8413-11e9-91b0-13f37d992530")
            {
                //Console.WriteLine("derp");
            }

            // Skip this object if envelope is too far from vertex
            if (!outerObj.geometry.Envelope.IsWithinDistance(new Point(innerVertex), epsilon)) continue;

            if (GeometryHelper.GetIndexInGeometry(innerVertex, outerObj.geometry) >= 0)
                return innerVertex;


            // Loop through outlines in object
            foreach (var outer in outerObj.geometry.Coordinates)
            {
                // Case 0: Vertex-Vertex direct match
                if (innerVertex.Equals(outer))
                {
                    //if (debug) //Console.WriteLine("Matching vertex found for {0}", innerVertex.ToString());
                    matchState = MatchState.Direct;
                    matchingObject = outerObj;
                }
                else
                {
                    // Case 1: Vertex-Vertex close match
                    double dist = innerVertex.Distance(outer);
                    if (dist < epsilon && dist < minDist)
                    {
                        // Skip coordinates that already exist in the current object, otherwise very prone to self-intersection with close vertices in the same object
                        if (GeometryHelper.GetIndexInGeometry(outer, geom) >= 0)
                            continue;

                        minDist = dist;
                        matchState = MatchState.VertexVertex;
                        matchingObject = outerObj;
                        //matchingVertexFound = true;
                        matchingVertex = outer;
                    }
                }
            }
        }

        // Here, all objects have been checked, so do whatever needs doing
        if (matchState == MatchState.VertexVertex)
        {
            // TODO Move all vertices here?
            if (debug) //Console.WriteLine("Close vertex found for {0} with {1}, moving inner vertex to outer. Object id: {2}", innerVertex.ToString(), matchingVertex.ToString(), matchingObject.ID);
            MoveVertex(innerObjects, innerVertex, matchingVertex);
            return matchingVertex;
        }
        return innerVertex;
    }

    // Moves a vertex to another position, if available
    private void MoveVertex(List<SimpleBGTObject> objects, Coordinate currentVector, Coordinate newPosition)
    {
        NetTopologySuite.Geometries.Point p = new NetTopologySuite.Geometries.Point(currentVector);
        foreach (var obj in objects)
        {
            // Check whether the object contains the current vertex
            bool contained = false;

            if (obj.geometry.Touches(p))
                contained = true;

            if (!contained) continue;

            ////Console.WriteLine("Object {0} contains the point.", obj.ID);

            // Replace the current coordinate in the geometry by the new one
            obj.geometry = GeometryHelper.ReplaceCoordinate(obj.geometry, currentVector, newPosition);

            obj.geometry.GeometryChanged();
        }
    }

    private void ProcessEdges(Geometry g, List<SimpleBGTObject> innerObjects, List<SimpleBGTObject> outerObjects, float epsilon = EPSILON)
    {
        bool debug = true;

        var coordinates = g.Coordinates.ToList();


        foreach (var poly in PolygonExtracter.GetPolygons(g))
        {
            ////Console.WriteLine("Cluster polygon is CCW?: {0}", ((Polygon)poly).Shell.IsCCW);
        }

        //for (int i = 0; i < coordinates.Count - 1; i++)
        int i = 0;
        while (i < coordinates.Count - 1)
        {
            LineString e = new LineString(new Coordinate[2] { coordinates[i], coordinates[i + 1] });
            int priority = 10000;
            Transformation t = null;

            double edgeLength = e.StartPoint.Distance(e.EndPoint);

            drawer.currentEdge = e;

            foreach (var obj in outerObjects)
            {
                if (obj.geometry.Distance(e.Envelope) > epsilon)
                    continue;

                if (obj.geometry.Area == 0)
                    continue;

                if (obj.ID == "b6b84ac30-d556-11e7-8ec4-89be260623ee" || obj.ID == "b8fbb0362-d54f-11e7-951f-610a7ca84980")
                {
                    //Console.WriteLine("derp");
                }

                Geometry outerGeom = obj.geometry;

                // Outer object is too far away
                if (outerGeom.Distance(e) > epsilon)
                    continue;

                List<int> invalidIndices = new List<int>();
                var polygons = PolygonExtracter.GetPolygons(outerGeom);

                int coordCounter = 0;

                // Create list of invalid indices (neighboring coordinates that are part of separate geometries)
                for (int j = 0; j < polygons.Count; j++)
                {
                    Polygon poly = (Polygon)polygons[j];

                    coordCounter += poly.ExteriorRing.Count;
                    invalidIndices.Add(coordCounter - 1);

                    foreach (var hole in poly.Holes)
                    {
                        coordCounter += hole.Count;
                        invalidIndices.Add(coordCounter - 1);
                    }
                }

                //int index = GeometryHelper.GetIndexInGeometry(e[0], outer, false);
                int index = GeometryHelper.GetIndexInGeometry(e[0], outerGeom);

                int endIndex = GeometryHelper.GetIndexInGeometry(e[1], outerGeom, false);

                // Case 0: Edge exists in geometry
                // Result: No need to loop through outer objects anymore, break the loop.
                if (GeometryHelper.ContainsEdge(outerGeom, e))
                {
                    priority = -1;
                    break;
                }

                // TODO: Deal with holes in this section

                // Index >= 0 means the outer object contains the first vertex
                if (index >= 0 && !invalidIndices.Contains(index)) 
                {
                    //int index = GeometryHelper.GetIndexInGeometry(e[0], outer);

                    if (index < 0)
                        throw new Exception("Index in geometry < 0, this should not happen!");

                    // The index of the next coordinate should never go out of bounds because it either sits in the middle or is the first index
                    int nextIndex = index + 1;

                    NetTopologySuite.Geometries.Point nextPoint = new NetTopologySuite.Geometries.Point(outerGeom.Coordinates[nextIndex]);

                    // Case 3
                    if (e.Distance(nextPoint) < epsilon// && !g.Touches(previous)
                        && !coordinates.Contains(nextPoint.Coordinate)        // Not sure what this does, maybe avoids snapping to points on other side? Seems crappy handling of case 0
                        && nextPoint.Distance(e.StartPoint) < edgeLength    // These conditions ensure that the next point is not too far outside; should be processed by next point otherwise
                        && nextPoint.Distance(e.EndPoint) < edgeLength)     // TODO: Check when this is necessary and if this may stop some valid matches
                    {
                        // TODO: Check if this causes self-intersection

                        priority = 0;
                        // AddVertexBetween(E, Outer[E.s].previous, 0, T)
                        t = AddVertexBetween(e, nextPoint, 0, t, "inner");
                        if (debug)
                            //Console.WriteLine("Case 3: Adding vertex between {0} and {1} at {2}", e[0], e[1], nextPoint);
                        break;
                    }

                    if (priority < 1) continue;

                    LineString o = new LineString(new Coordinate[] { outerGeom.Coordinates[index], outerGeom.Coordinates[index + 1] });

                    if (e.Distance(nextPoint) >= epsilon
                        && GeometryHelper.GetIndexInGeometry(e.EndPoint.Coordinate, outerGeom) == -1)   // Ensure that the endpoint does not already exist in the other geometry (avoids infinite loop)
                    {
                        // Case 5
                        if (o.Distance(e.EndPoint) < epsilon && e.EndPoint.Distance(o.StartPoint) < o.Length
                            && e.EndPoint.Distance(o.EndPoint) < o.Length)   // TODO: Added && to make sure it lies -on- the edge, dot product is probably better
                        {
                            bool alreadyExists = false;
                            alreadyExists = GeometryHelper.ContainsEdge(coordinates, o);

                            if (!alreadyExists)
                            {
                                priority = 1;
                                // AddVertexBetween(O, E.e, 1, T)
                                t = AddVertexBetween(o, e.EndPoint, 1, t, obj.ID);
                                if (debug)
                                    //Console.WriteLine("Case 5: Adding vertex between {0} and {1} at {2} on object {3}", o[0], o[1], e[1], obj.ID);
                                break;
                            }
                        }
                    }
                }

                if (priority < 2) continue;

                for (int j = 0; j < outerGeom.Coordinates.Length - 1; j++)
                {
                    // Continue if these vertices are at start or end of different holes, meaning they are not an actual edge
                    if (invalidIndices.Contains(j))
                        continue;

                    LineString o = new LineString(new Coordinate[] { outerGeom.Coordinates[j], outerGeom.Coordinates[j + 1] });

                    bool alreadyExists = false;

                    // If o exists in coordinates, skip!
                    for (int k = 0; k < coordinates.Count - 1; k++)
                    {
                        if (coordinates[k].Equals(outerGeom.Coordinates[j]))
                        {
                            if (coordinates[k + 1].Equals(outerGeom.Coordinates[j + 1]))
                                alreadyExists = true;
                            else if(k > 0)
                            {
                                if (coordinates[k - 1].Equals(outerGeom.Coordinates[j + 1]))
                                    alreadyExists = true;
                            }
                        }
                    }

                    if (alreadyExists) continue;

                    double outerLength = o.StartPoint.Distance(o.EndPoint);
                    bool ES = o.Distance(e.StartPoint) < epsilon;
                    bool OS = e.Distance(o.StartPoint) < epsilon;

                    // Case 8, 9, 13
                    if (ES)
                    {
                        // This is an edge case problem to counter selfe intersection
                        if (outerGeom.Touches(e.StartPoint))
                            continue;

                        // Make sure the vertex is "on" the edge
                        if (e.StartPoint.Distance(o.StartPoint) >= outerLength || e.StartPoint.Distance(o.EndPoint) >= outerLength)
                            continue;

                        // TODO: Check if this causes self-intersection

                        priority = 2;
                        // AddVertexBetween(O, E.s, 2, T)
                        t = AddVertexBetween(o, e.StartPoint, 2, t, obj.ID);
                        break;
                    }

                    // Case 11
                    else if (OS && priority >= 3// && !g.Touches(o.StartPoint))   // TODO: The second predicate is edge case handling: DOCUMENT
                        && !coordinates.Contains(o.StartPoint.Coordinate))   // TODO: now using coordinates instead of g
                    {
                        // Make sure the vertex is "on" the edge
                        if (o.StartPoint.Distance(e.StartPoint) >= edgeLength || o.StartPoint.Distance(e.EndPoint) >= edgeLength)
                            continue;

                        // TODO: Check if this causes self-intersection

                        priority = 3;
                        // AddVertexBetween(E, O.s, 3, T)
                        t = AddVertexBetween(e, o.StartPoint, 3, t, "inner");
                        if (debug)
                            //Console.WriteLine("Case 11: Adding vertex between {0} and {1} at {2}", e[0], e[1], o[0]);
                        break;
                    }
                }
            }

            if (t != null && priority >= 0)
            {
                RepairEdge(t, outerObjects, innerObjects);

                // 3: t.e == E, add v to e, i++
                // 5: t.e == O, add v to o, i++
                // 8: t.e == O, add v to o, no i++
                // 9: t.e == O, add v to o, no i++
                // 11: t.e == e, add v to e, i++
                // 13: t.e = O, add v to o, no i++

                if (t.e.Equals(e))
                {
                    if (priority == 0)
                    {
                        coordinates.Insert(i + 1, t.v.Coordinate);
                        i++;
                    }
                    else if (priority == 1)
                    {
                        // TODO: Inverse order means rechecking because it should turn into a case 3, priority 0 case next time if done
                        //i++;
                    }
                    else if (priority == 2)
                    {
                        // No i++, recheck same vertex
                        //i++; // TODO: Remove this
                    }
                    else if (priority == 3)
                    {
                        coordinates.Insert(i + 1, t.v.Coordinate);
                        i++;
                    }
                }
            }
            // Either no transformation or a -1 transformation, so move on with the next one.
            else i++;
        }
    }

    public Transformation AddVertexBetween(LineString e, NetTopologySuite.Geometries.Point v, int p, Transformation t, string id = "")
    {
        if (e.Distance(v) > 0.3)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            //Console.WriteLine("Error: Distance {0} too big!", e.Distance(v));
            Console.ForegroundColor = ConsoleColor.White;
        }

        if (t == null)
            return new Transformation(e, v, p, id);
        if (p < t.p)
            return new Transformation(e, v, p, id);
        if (p == t.p)
        {
            if (p == 0)
            {
                if (v.Distance(e.StartPoint) < t.e.StartPoint.Distance(t.v))
                {
                    return new Transformation(e, v, p, id);
                }
            }
            // Testing if inverse distance metric works for case 5 (priority 1)
            else if (p == 1)
            {
                if (e.StartPoint.Distance(v) > t.e.StartPoint.Distance(t.v))
                    return new Transformation(e, v, p, id);
            }
            else if (e.StartPoint.Distance(v) < t.e.StartPoint.Distance(t.v))
            {
                return new Transformation(e, v, p, id);
            }
        }
        return t;
    }

    public void RepairEdge(Transformation t, List<SimpleBGTObject> outer, List<SimpleBGTObject> inner)
    {
        Console.ForegroundColor = ConsoleColor.Cyan;
        //Console.WriteLine("Applied transformation has priority {0}", t.p);
        Console.ForegroundColor = ConsoleColor.White;

        if (t.e.Distance(t.v) > 0.3)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            //Console.WriteLine("Error: Distance {0} too big!", t.e.Distance(t.v));
            Console.ForegroundColor = ConsoleColor.White;
        }

        foreach (var o in outer)
        {
            if (o.geometry.Distance(t.e) < EPSILON)
            {
                if (!o.geometry.IsValid)
                {
                    var isValidOp = new NetTopologySuite.Operation.Valid.IsValidOp(o.geometry);
                    if (!isValidOp.IsValid)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        //Console.WriteLine("ERROR: Geometry is not valid:" + isValidOp.ValidationError.Message);
                        //Console.WriteLine("Object ID: " + o.ID);
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                }

                if (o.geometry.Covers(t.e) && o.geometry.Touches(t.e))
                {
                    Geometry g = o.geometry;
                    //Console.WriteLine("RepairEdge: Adding vertex in OUTER between {0} and {1} at {2}", t.e[0], t.e[1], t.v);

                    drawer.otherEdge = t.e;
                    drawer.newVertex = t.v.Coordinate;

                    if (DRAWER_ENABLED)
                    {
                        pictureBox.Refresh();
                    }

                    int countPrevious = o.geometry.Coordinates.Length;
                    //Console.WriteLine("Coordinates length: {0}", countPrevious);

                    // TODO: Check how we deal with indices at start or end of geometries / holes below
                    int indexStart = GeometryHelper.GetIndexInGeometry(t.e[0], o.geometry);
                    int indexEnd = GeometryHelper.GetIndexInGeometry(t.e[1], o.geometry);
                    //Console.WriteLine("Indices: {0}, {1}", indexStart, indexEnd);

                    if (indexStart < 0 || indexEnd < 0)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        //Console.WriteLine("ERROR: index below 0, should not happen!");
                        //Console.WriteLine("Object ID: " + o.ID);
                        Console.ForegroundColor = ConsoleColor.White;
                    }

                    Geometry newGeom = GeometryHelper.AddBetween(o.geometry, t.e[0], t.e[1], t.v.Coordinate);
                    o.geometry = newGeom;
                    o.geometry.GeometryChanged();

                    //o.geometry = GeometryHelper.AddBetween(o.geometry, t.e[0], t.e[1], t.v.Coordinate);
                    //o.geometry.GeometryChanged();
                    if (o.geometry.Coordinates.Length == countPrevious)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        //Console.WriteLine("ERROR: No vertex was added to geometry.");
                        //Console.WriteLine("Object ID: " + o.ID);
                        Console.ForegroundColor = ConsoleColor.White;
                    }

                    //if (!o.geometry.IsValid)
                    if (!newGeom.IsValid)
                    {
                        var isValidOp = new NetTopologySuite.Operation.Valid.IsValidOp(newGeom);
                        if (!isValidOp.IsValid)
                        {
                            //drawer.CenterOnInner();
                            if (DRAWER_ENABLED)
                            {
                                drawer.SetFocus(t.e);
                                pictureBox.Refresh();
                            }

                            Console.ForegroundColor = ConsoleColor.Yellow;
                            //Console.WriteLine("ERROR: Geometry is not valid:" + isValidOp.ValidationError.Message);
                            //Console.WriteLine("Object ID: " + o.ID);
                            Console.ForegroundColor = ConsoleColor.White;
                        }
                    }
                    else
                    {
                        //o.geometry = newGeom;
                        //o.geometry.GeometryChanged();
                    }
                }
            }
        }

        foreach (var o in inner)
        {
            if (o.geometry.Envelope.Distance(t.e) < 0.1)
            {
                if (!o.geometry.IsValid)
                {
                    var isValidOp = new NetTopologySuite.Operation.Valid.IsValidOp(o.geometry);
                    if (!isValidOp.IsValid)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        //Console.WriteLine("ERROR: Geometry is not valid:" + isValidOp.ValidationError.Message);
                        //Console.WriteLine("Object ID: " + o.ID);
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                }

                if (o.geometry.Covers(t.e) && o.geometry.Touches(t.e))
                {
                    Geometry g = o.geometry;
                    //Console.WriteLine("RepairEdge: Adding vertex in INNER between {0} and {1} at {2}", t.e[0], t.e[1], t.v);

                    drawer.otherEdge = t.e;
                    drawer.newVertex = t.v.Coordinate;

                    if (DRAWER_ENABLED)
                        pictureBox.Refresh();

                    int indexStart = GeometryHelper.GetIndexInGeometry(t.e[0], o.geometry);
                    int indexEnd = GeometryHelper.GetIndexInGeometry(t.e[1], o.geometry);
                    //Console.WriteLine("Indices: {0}, {1}", indexStart, indexEnd);

                    if (indexStart < 0 || indexEnd < 0)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        //Console.WriteLine("ERROR: index below 0, should not happen!");
                        //Console.WriteLine("Object ID: " + o.ID);
                        Console.ForegroundColor = ConsoleColor.White;
                    }

                    Geometry newGeom = GeometryHelper.AddBetween(o.geometry, t.e[0], t.e[1], t.v.Coordinate);
                    o.geometry = newGeom;
                    o.geometry.GeometryChanged();

                    //if (!o.geometry.IsValid)
                    if (!newGeom.IsValid)
                    {
                        var isValidOp = new NetTopologySuite.Operation.Valid.IsValidOp(newGeom);
                        if (!isValidOp.IsValid)
                        {
                            //Console.WriteLine("ERROR: Geometry is not valid:" + isValidOp.ValidationError.Message);
                            //Console.WriteLine("Edge length: {0}", t.e.StartPoint.Distance(t.e.EndPoint));
                            //Console.WriteLine("Vertex distance: start {0}, end {1}", t.e.StartPoint.Distance(t.v), t.e.EndPoint.Distance(t.v));
                            //Console.WriteLine("Object ID: " + o.ID);
                        }
                    }
                    else {
                        //o.geometry = newGeom;
                        //o.geometry.GeometryChanged();
                    }
                }
            }
        }
    }
    #endregion
}
