﻿using NetTopologySuite.Geometries;

public class Edge
{
    public Coordinate start, end;

    public Edge(Coordinate a, Coordinate b)
    {
        this.start = a;
        this.end = b;
    }

    public static bool operator ==(Edge e1, Edge e2)
    {
        return (e1.start.Equals(e2.start) && e1.end.Equals(e2.end)
            || e1.start.Equals(e2.end) && e1.end.Equals(e2.start));
    }

    public static bool operator !=(Edge e1, Edge e2)
    {
        return !(e1.Equals(e2));
    }
}