﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using Npgsql;
using NetTopologySuite;
using NetTopologySuite.Geometries;
using NetTopologySuite.Geometries.Utilities;

static class DBHelper
{
    private static NpgsqlConnection pgConn;

    private const int EPSG = 28992;

    public static string SCHEMA = "public.";

    public static NpgsqlConnection GetPostgresConnection()
    {
        //if (conn == null)
        pgConn = new NpgsqlConnection("Host=localhost;Port=5432;Username=testuser;Password=testpassword;Database=bgtdb");     // This is hardcoded; Allow the user to enter the database location as an argument
        return pgConn;
    }



    public static string GetCityObjectInsertString()
    {
        return "INSERT INTO Object (ID, Type, RelativeHeight, SurfaceMaterial, PhysicalAppearance, Function, Class) VALUES(@ID, @Type, @RelativeHeight, @SurfaceMaterial, @PhysicalAppearance, @Function, @Class)" +
            "ON CONFLICT(ID)" +
            "DO UPDATE SET " +
            "Type=excluded.Type, " +
            "RelativeHeight=excluded.RelativeHeight, " +
            "SurfaceMaterial=excluded.SurfaceMaterial, " +
            "PhysicalAppearance=excluded.PhysicalAppearance, " +
            "Function=excluded.Function, " +
            "Class=excluded.Class";
    }

    public static string GetGeometryInsertString()
    {
        return "INSERT INTO Geometry (ObjectID, Outlines, Interiors, MinX, MaxX, MinZ, MaxZ, Type) VALUES(@ObjectID, @Outlines, @Interiors, @MinX, @MaxX, @MinZ, @MaxZ, @Type)" +
            "ON CONFLICT(ObjectID)" +
            "DO UPDATE SET " +
            "Outlines=excluded.Outlines, " +
            "Interiors=excluded.Interiors, " +
            "MinX=excluded.MinX, " +
            "MaxX=excluded.MaxX, " +
            "MinZ=excluded.MinZ, " +
            "MaxZ=excluded.MaxZ," +
            "Type=excluded.Type";
    }

    public static List<SimpleBGTObject> GetAllObjectsPostGIS(string type, NpgsqlConnection conn, Envelope window, bool test = false)
    {
        List<SimpleBGTObject> resultList = new List<SimpleBGTObject>();

        // TODO THIS IS TEST MATERIAL STUFF PLS REMOVE test. SCHEMA
        string selectCmd;

        if (test)
        {
           selectCmd =
           "SELECT * " +
           "FROM test." + type + " " +
           "WHERE geometrie && ST_MakeEnvelope(@minx, @miny, @maxx, @maxy, @epsg);";
        }
        else
        {
           selectCmd =
           "SELECT * " +
           "FROM " + SCHEMA + type + " " +
           "WHERE geometrie && ST_MakeEnvelope(@minx, @miny, @maxx, @maxy, @epsg);";
        }

        var cmd = new NpgsqlCommand(selectCmd, conn);

        // cmd.Parameters.AddWithValue("@type", type);
        cmd.Parameters.AddWithValue("@minx", (int)window.MinX);
        cmd.Parameters.AddWithValue("@miny", (int)window.MinY);
        cmd.Parameters.AddWithValue("@maxx", (int)window.MaxX);
        cmd.Parameters.AddWithValue("@maxy", (int)window.MaxY);
        cmd.Parameters.AddWithValue("@epsg", EPSG);

        NpgsqlDataReader reader = cmd.ExecuteReader();

        while (reader.Read())
        {
            SimpleBGTObject obj = new SimpleBGTObject();
            obj.ID = (string)reader["gml_id"];
            obj.relativeHeight = reader.GetInt32(reader.GetOrdinal("relatievehoogteligging"));
            obj.geometry = (Geometry)reader.GetValue(reader.GetOrdinal("geometrie"));
            obj.type = type;

            switch (obj.geometry.GeometryType)
            {
                case "Polygon":
                    obj.polygon = (Polygon)obj.geometry;
                    break;
                case "MultiPolygon":
                    obj.multiPolygon = (MultiPolygon)obj.geometry;
                    break;
                case "Point":
                    obj.point = (Point)obj.geometry;
                    break;
                case "LineString":
                    obj.lineString = (LineString)obj.geometry;
                    break;
                default: break;
            }

            resultList.Add(obj);
        }

        reader.Dispose();

        return resultList;
    }

    public static List<SimpleBGTObject> GetAllPostGIS(NpgsqlConnection conn, Envelope window, bool test = false)
    {
        conn.Open();

        string[] bgtObjectTypes = new string[] { "wegdeel", "pand", "overbruggingsdeel", "tunneldeel", "onbegroeidterreindeel", "begroeidterreindeel", "waterdeel", "ondersteunendwegdeel", "scheiding", "ondersteunendwaterdeel", "kunstwerkdeel", "paal", "vegetatieobject" };

        List<SimpleBGTObject> allObjects = new List<SimpleBGTObject>();

        foreach (var type in bgtObjectTypes)
        {
            var objects = GetAllObjectsPostGIS(type, conn, window, test);
            allObjects.AddRange(objects);
        }

        conn.Close();

        return allObjects;
    }

    // Takes a list of connections and upserts them into the db
    public static void UpsertConnections(List<Connection> connections)
    {
        var conn = GetPostgresConnection();
        conn.Open();

        string insertCmd =
            "INSERT INTO " + SCHEMA + "connection (id_a, id_b, index_a, layer_a, layer_b, edge) VALUES(@id_a, @id_b, @index_a, @layer_a, @layer_b, @edge)";

        var tr = conn.BeginTransaction();

        int counter = 0;

        foreach (var obj in connections)
        {
            var cmd = new NpgsqlCommand(insertCmd, conn);
            cmd.Parameters.AddWithValue("@id_a", obj.a.ID);
            cmd.Parameters.AddWithValue("@id_b", obj.b.ID);
            cmd.Parameters.AddWithValue("@index_a", obj.indexA);
            cmd.Parameters.AddWithValue("@layer_a", obj.a.relativeHeight);
            cmd.Parameters.AddWithValue("@layer_b", obj.b.relativeHeight);
            cmd.Parameters.AddWithValue("@edge", obj.geometry);
            counter += cmd.ExecuteNonQuery();
        }

        tr.Commit();
        tr.Dispose();

        //Console.WriteLine(string.Format("Successfully added {0} connections to the database.", counter));

        conn.Close();
    }

    public static void UpsertAllPostGIS(Dictionary<string, SimpleBGTObject> objects)
    {
        Dictionary<string, List<SimpleBGTObject>> objDict = new Dictionary<string, List<SimpleBGTObject>>();

        // Populate dictionary entry for every separate object type
        foreach (var obj in objects)
        {
            if (!objDict.ContainsKey(obj.Value.type))
                objDict.Add(obj.Value.type, new List<SimpleBGTObject>());

            objDict[obj.Value.type].Add(obj.Value);
        }

        pgConn.Open();

        foreach (var item in objDict)
        {
            NpgsqlTransaction tr = pgConn.BeginTransaction();
            int counter = 0;

            foreach (var obj in item.Value)
            {
                var command = CreateCommand(obj, pgConn);
                counter += command.ExecuteNonQuery();
            }

            tr.Commit();
            tr.Dispose();

            //Console.WriteLine(string.Format("Successfully added {0} objects to the database.", counter));
        }

        pgConn.Close();
    }

    public static NpgsqlCommand CreateCommand(SimpleBGTObject o, NpgsqlConnection c)
    {
        string insertString = InsertStrings.GetInsertString(o.type);
        var cmd = new NpgsqlCommand(insertString, c);
        cmd.Parameters.AddWithValue("id", o.ID);
        cmd.Parameters.AddWithValue("geometry", o.geometry);
        cmd.Parameters.AddWithValue("height", o.relativeHeight);
        return cmd;
    }
}
