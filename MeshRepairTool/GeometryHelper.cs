﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System;
using NetTopologySuite.Geometries;
using NetTopologySuite.Geometries.Utilities;

public static class GeometryHelper
{
    private const int SCALING_FACTOR = 1000000;

    /*
    public static List<Path> GetOutlinesFromObjectList(List<SimpleBGTObject> transforms)
    {
        List<Path> outlines = new List<Path>();

        // Add all outlines of separate object to a single path list
        foreach (var trans in transforms)
        {
            foreach (List<Coordinate> outline in trans.geometry.outlines)
            {
                outlines.Add(OutlineToIntpoints(outline));
            }
        }
        return outlines;
    }

    public static List<CityObjectMember> GetSurroundingObjects(List<Coordinate> outline, List<CityObjectMember> surroundings, float epsilon = 0)
    {
        List<CityObjectMember> result = new List<CityObjectMember>();

        foreach (var outer in surroundings)
        {
            if (outer.bounds.Intersects(outline, epsilon))
            {
                result.Add(outer);
            }
        }

        return result;
    }
    

    public static List<CityObjectMember> GetSurroundingObjects(Coordinate point, List<CityObjectMember> surroundings, float epsilon = 0)
    {
        List<CityObjectMember> result = new List<CityObjectMember>();

        foreach (var outer in surroundings)
        {
            if (outer.bounds.Intersects(point, epsilon))
            {
                result.Add(outer);
            }
        }

        return result;
    }

        */

    public static string OutlinesToString(List<List<Coordinate>> lines)
    {
        string result = "";
        List<string> lineStrings = new List<string>();
        foreach (var line in lines)
        {
            string lineString = "";
            foreach (var vec in line)
            {
                double xFloat = vec.X;
                double zFloat = vec.Z;
                string x = xFloat.ToString("R");    // Roundtrip formatting to ensure same result upon reading
                string z = zFloat.ToString("R");

                lineString += x + " " + z + " ";
            }
            lineStrings.Add(lineString.Trim());
        }
        result = String.Join(";", lineStrings);
        return result;
    }

    public static List<List<Coordinate>> OutlinesFromString(string source)
    {
        string[] split = source.Split(';');
        List<List<Coordinate>> result = new List<List<Coordinate>>();

        foreach (var lineString in split)
        {
            result.Add(StringToLine(lineString));
        }

        return result;
    }

    // Turns a linestring into a list of vectors. TODO: use the supplied xOffset and zOffset
    public static List<Coordinate> StringToLine(string lineString, int xOffset = 0, int zOffset = 0)
    {
        List<Coordinate> line = new List<Coordinate>();

        string[] split = lineString.Split(' ');

        for (int i = 0; i < split.Length / 2; i++)
        {
            int idx = i * 2;
            float x = float.Parse(split[idx]) - xOffset;
            float z = float.Parse(split[idx + 1]) - zOffset;

            line.Add(new Coordinate(x, z));
        }
        return line;
    }

    public static NetTopologySuite.Geometries.Geometry GeometryFromOutlinesHoles(List<List<Coordinate>> outlines, List<List<Coordinate>> holes)
    {
        List<NetTopologySuite.Geometries.LinearRing> outlineLineStrings = outlines.Select(x => LineStringFromCoordinateList(x)).ToList();
        List<NetTopologySuite.Geometries.LinearRing> holeLineStrings = holes.Select(x => LineStringFromCoordinateList(x)).ToList();


        GeometryFactory factory = new GeometryFactory();
        factory.CreatePolygon();

        return null;
    }

    public static NetTopologySuite.Geometries.MultiPolygon MultiPolygonFromPolygons(List<NetTopologySuite.Geometries.Polygon> polygons)
    {
        return new MultiPolygon(polygons.ToArray());
    }

    public static NetTopologySuite.Geometries.LinearRing LineStringFromCoordinateList(List<Coordinate> coordinateList)
    {
        return new NetTopologySuite.Geometries.LinearRing(coordinateList.ToArray());
    }

    public static NetTopologySuite.Geometries.LinearRing[] LinearRingArrayFromCoordinateLists(List<List<Coordinate>> coordinateLists)
    {
        return coordinateLists.Select(x => LineStringFromCoordinateList(x)).ToArray();
    }

    /*
    public static Edge GetEdge(List<Coordinate> input, int index)
    {
        return new Edge(input[index], input[(index + 1) % input.Count]);
    }*/

    public static Edge GetEdge(Coordinate[] input, int index)
    {
        return new Edge(input[index], input[(index + 1) % input.Length]);
    }

    // https://stackoverflow.com/questions/30559799/function-for-finding-the-distance-between-a-point-and-an-edge-in-java
    public static double distPointLine(Coordinate point, Coordinate lineStart, Coordinate lineEnd)
    {
        double AB = point.Distance(lineStart);
        double BC = lineStart.Distance(lineEnd);
        double AC = point.Distance(lineEnd);

        // Heron's formula
        double s = (AB + BC + AC) / 2;
        double area = Math.Sqrt(s * (s - AB) * (s - BC) * (s - AC));

        double AD = (2 * area) / BC;
        return AD;
    }

    //https://stackoverflow.com/questions/52468343/finding-shortest-distance-between-a-point-and-a-line-segment
    public static double GetDist(Coordinate a, Coordinate b, Coordinate point)
    {
        double ax = a.X;
        double ay = a.Y;
        double bx = b.X;
        double by = b.Y;
        double x = point.X;
        double y = point.Y;

        if ((ax - bx) * (x - bx) + (ay - by) * (y - by) <= 0)
            return Math.Sqrt((x - bx) * (x - bx) + (y - by) * (y - by));

        if ((bx - ax) * (x - ax) + (by - ay) * (y - ay) <= 0)
            return Math.Sqrt((x - ax) * (x - ax) + (y - ay) * (y - ay));

        return Math.Abs((by - ay) * x - (bx - ax) * y + bx * ay - by * ax) /
            Math.Sqrt((ay - by) * (ay - by) + (ax - bx) * (ax - bx));
    }

    public static Geometry ReplaceCoordinate(Geometry g, Coordinate oldCoord, Coordinate newCoord)
    {
        GeometryEditor editor = new GeometryEditor();
        var op = new CoordinateReplacerOperation(oldCoord, newCoord);
        return editor.Edit(g, op);
    }

    public static Geometry AddBetween(Geometry g, Coordinate edgeStart, Coordinate edgeEnd, Coordinate vertex)
    {
        GeometryEditor editor = new GeometryEditor();
        var op = new AddVertexOnEdgeOperation(edgeStart, edgeEnd, vertex);
        ////Console.WriteLine("Input coordinates length: {0}", g.Coordinates.Length);
        ////Console.WriteLine("Input geometry count: {0}", g.NumGeometries);
        if (g.GeometryType == "Polygon")
        {
            ////Console.WriteLine("Input hole count: {0}", ((Polygon)g).Holes.Length);
        }


        if (g.Coordinates.Length > 200)
        {
            ////Console.WriteLine("Length is more than 200");
        }
        return editor.Edit(g, op);
    }

    /*public static bool ContainsEdge(Geometry g, LineString e)
    {
        List<int> matchingIndices = new List<int>();

        for (int i = 0; i < g.Coordinates.Length; i++)
        {
            Coordinate c = (Coordinate)g.Coordinates[i];
            if (c.Equals(e[0])) matchingIndices.Add(i);
        }

        if (matchingIndices.Count == 0) return false;

        foreach (int index in matchingIndices)
        {
            if (index < g.Coordinates.Length - 1)
            {
                if (g.Coordinates[index + 1].Equals(e[1]))
                    return true;
            }

            if (index > 0)
            {
                if (g.Coordinate[index - 1].Equals(e[1]))
                    return true;
            }
        }

        return false;
    }*/

    public static int GeometryEdgeMatches(Geometry g, LineString e)
    {
        int result = 0;
        bool foundStart = false;
        bool foundEnd = false;

        foreach (var c in g.Coordinates)
        {
            if (c.Equals(e[0]))
                foundStart = true;
            if (c.Equals(e[1]))
                foundEnd = true;
        }

        if (foundStart) result++;
        if (foundEnd) result++;

        return result;
    }

    public static bool VerticesBetweenWithinEpsilon(Geometry g, LineString e, float eps)
    {
        Coordinate start = e[0];
        Coordinate end = e[1];

        int startIndex = 0;
        int endIndex = 0;

        for (int i = 0; i < g.Coordinates.Length; i++)
        {
            // TODO: Handle multiple matches
            if (g.Coordinates[i].Equals(start) && startIndex == 0)
                startIndex = i;

            if (g.Coordinates[i].Equals(end) && endIndex == 0)
                endIndex = i;

            if (startIndex != 0 && endIndex != 0) break;
        }

        Point startPoint = new Point(start);
        Point endPoint = new Point(end);

        if (startIndex < endIndex)
        {
            for (int i = startIndex; i <= endIndex; i++)
            {
                Geometry p = new Point(g.Coordinates[i]);
                if (p.Distance(e) >= eps)
                    return false;
            }
            return true;
        }
        else
        {
            for (int i = endIndex; i <= startIndex; i++)
            {
                Geometry p = new Point(g.Coordinates[i]);
                if (p.Distance(e) >= eps)
                    return false;
            }
            return true;
        }
    }

    public static int GetIndexInGeometry(Coordinate c, Geometry g, bool allowFirst = true)
    {
        List<int> firstIndices = new List<int>();   // Coordinate indices of "first" coordinates of a polygon or hole.
        var polygons = PolygonExtracter.GetPolygons(g);

        int coordCounter = 0;

        if (!allowFirst)
        {
            firstIndices.Add(0);

            // Create list of invalid indices (neighboring coordinates that are part of separate geometries)
            for (int j = 0; j < polygons.Count; j++)
            {
                Polygon poly = (Polygon)polygons[j];

                coordCounter += poly.ExteriorRing.Count;

                foreach (var hole in poly.Holes)
                {
                    firstIndices.Add(coordCounter);
                    coordCounter += hole.Count;
                }
            }
        }

        for (int i = 0; i < g.Coordinates.Length; i++)
        {
            if (!allowFirst)
            {
                if (firstIndices.Contains(i)) continue;
            }

            if (g.Coordinates[i].Equals(c))
            {
                //if (i == 0)
                if (false)
                    return g.Coordinates.Length - 1;
                else return i;
            }
        }
        return -1;
    }


    // This function checks whether the coordinates of an edge sit next to each other in a geometry.
    // TODO: This does not hold into account whether this index is part of a whole. This should be doable by checking whether a found edge exists on an edge that 
    // contains the end of one geometry and the start of another.
    public static bool ContainsEdge(Geometry g, LineString e)
    {
        return ContainsEdge(g.Coordinates.ToList(), e);

        List<int> startIndices = new List<int>();
        List<int> endIndices = new List<int>();

        for (int i = 0; i < g.Coordinates.Length; i++)
        {
            if (g.Coordinates[i].Equals(e.StartPoint.Coordinate))
                startIndices.Add(i);

            else if (g.Coordinates[i].Equals(e.EndPoint.Coordinate))
                endIndices.Add(i);
        }

        foreach(int start in startIndices)
        {
            foreach(int end in endIndices)
            {
                if (Math.Abs(end - start) == 1)
                    return true;
            }
        }

        return false;
    }

    public static bool ContainsEdge(List<Coordinate> g, LineString e)
    {
        List<int> startIndices = new List<int>();
        List<int> endIndices = new List<int>();

        for (int i = 0; i < g.Count; i++)
        {
            if (g[i].Equals(e.StartPoint.Coordinate))
                startIndices.Add(i);

            else if (g[i].Equals(e.EndPoint.Coordinate))
                endIndices.Add(i);
        }

        foreach (int start in startIndices)
        {
            foreach (int end in endIndices)
            {
                if (Math.Abs(end - start) == 1)
                    return true;
            }
        }

        return false;
    }
}

public class CoordinateReplacerOperation : GeometryEditor.CoordinateOperation
{
    private Coordinate original;
    private Coordinate changeTo;

    public CoordinateReplacerOperation(Coordinate original, Coordinate changeTo)
    {
        this.original = original;
        this.changeTo = changeTo;
    }

    public override Coordinate[] Edit(Coordinate[] coordinates, Geometry geometry)
    {
        if (coordinates.Length == 0)
            return null;

        Coordinate[] newCoords = new Coordinate[coordinates.Length];

        for (int i = 0; i < coordinates.Length; i++)
        {
            Coordinate coord = new Coordinate(coordinates[i]);
            if (coord.Equals(original))
            {
                ////Console.WriteLine("Changing coordinates");
                newCoords[i] = changeTo;
            }
            else newCoords[i] = coord;
        }

        return newCoords;
    }
}

public class AddVertexOnEdgeOperation : GeometryEditor.CoordinateOperation
{
    private Coordinate edgeStart, edgeEnd, vertex;

    public AddVertexOnEdgeOperation(Coordinate edgeStart, Coordinate edgeEnd, Coordinate vertex)
    {
        this.edgeStart = edgeStart;
        this.edgeEnd = edgeEnd;
        this.vertex = vertex;
    }

    public override Coordinate[] Edit(Coordinate[] coordinates, Geometry geometry)
    {
        if (coordinates.Length == 0)
            return null;

        ////Console.WriteLine("CoordinateOperation coordinates length: {0}", coordinates.Length);


        if (coordinates.Contains(vertex))
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            //Console.WriteLine("Error: Vertex already exists in object!");
            Console.ForegroundColor = ConsoleColor.White;
            return coordinates;
        }

        Coordinate[] newCoords = new Coordinate[coordinates.Length];

        List<Coordinate> coordsList = coordinates.ToList();

        bool added = false;

        for (int i = 0; i < coordsList.Count - 1; i++)
        {
            if (coordsList[i].Equals(edgeStart) && coordsList[i + 1].Equals(edgeEnd)
                || coordsList[i].Equals(edgeEnd) && coordsList[i + 1].Equals(edgeStart))
            {
                coordsList.Insert(i + 1, vertex);
                added = true;
                //Console.WriteLine("Added vertex at index {0}", i+1);
                break;
            }
        }

        /*if (!added)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            //Console.WriteLine("Error: No vertex added!");
            Console.ForegroundColor = ConsoleColor.White;
        }*/

        return coordsList.ToArray();
    }
}

public class EdgeMatchResult
{
    List<int> startIndices;
    List<int> endIndices;
}