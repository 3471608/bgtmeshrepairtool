﻿using NetTopologySuite.Geometries;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

public class DrawnObjects
{
    bool debug = false;
    public List<SimpleBGTObject> allInnerObjects;
    public List<SimpleBGTObject> currentInnerObjects;

    public List<SimpleBGTObject> allOuterObjects;
    public List<SimpleBGTObject> currentOuterObjects;

    public Geometry currentEdge;
    public Geometry otherEdge;
    public Coordinate newVertex;

    public int offsetX;
    public int offsetY;
    public float scalingFactor;
    public int padding = 10;

    private int halfWidth;
    private int halfHeight;

    public PictureBox p;


    public void SetOffsets()
    {
        Envelope e = new Envelope();

        if (allInnerObjects != null)
        {
            foreach (var o in allInnerObjects)
                e.ExpandToInclude(o.geometry.EnvelopeInternal);
        }

        if (allOuterObjects != null)
        {
            foreach (var o in allOuterObjects)
                e.ExpandToInclude(o.geometry.EnvelopeInternal);
        }

        halfWidth = p.Width / 2;
        halfHeight = p.Height / 2;

        offsetX = (int)e.Centre.X;
        offsetY = (int)e.Centre.Y;

        //Console.WriteLine("Offsets: {0}, {1}", offsetX, offsetY);

        int dataWidth = (int)(e.MaxX - e.MinX);
        int dataHeight = (int)(e.MaxY - e.MinY);

        int maxExtents = Math.Max(dataWidth, dataHeight);

        //Console.WriteLine("Extents w/h: {0}, {1}", dataWidth, dataHeight);
        //Console.WriteLine("Picturebox w/h: {0}, {1}", p.Width, p.Height);

        scalingFactor = 0.5f / (maxExtents / (float)Math.Max(p.Width - padding, p.Height - padding));
        //Console.WriteLine("Scaling factor: {0}", scalingFactor);

        if (scalingFactor > 500)
            scalingFactor = 1;
    }

    public void Draw(PaintEventArgs e)
    {
        Graphics g = e.Graphics;

        if (allOuterObjects != null)
        {
            using (var pen = new Pen(Color.Red, 1f))
            {
                foreach (var o in allOuterObjects)
                {
                    if (o.ID == "b4ef3a8eb-5423-11e8-951f-610a7ca84980")
                    {
                        //Console.WriteLine(o.ID);
                    }

                    for (int i = 0; i < o.geometry.Coordinates.Length - 1; i++)
                    {
                        PointF a = CoordToPoint(o.geometry.Coordinates[i]);
                        PointF b = CoordToPoint(o.geometry.Coordinates[i + 1]);

                        if (a.X > 10000 || a.Y > 10000 || b.X > 10000 || b.Y > 10000) continue;
                        if (a.X < -10000 || a.Y < -10000 || b.X < -10000 || b.Y < -10000) continue;

                        if (debug)
                            Console.WriteLine("Drawing point at {0}", a);

                        g.DrawLine(pen, a, b);

                        Rectangle rect = new Rectangle((int)a.X - 4, (int)a.Y - 4, 8, 8);
                        try
                        {
                            g.DrawRectangle(pen, rect);
                        }
                        catch(Exception exc)
                        {
                            Console.WriteLine("Something went wrong while drawing a rectangle.. {0}", exc);
                        }
                    }
                }
            }
        }

        if (allInnerObjects != null)
        {
            using (var pen = new Pen(Color.Blue, 1f))
            {
                foreach (var o in allInnerObjects)
                {
                    for (int i = 0; i < o.geometry.Coordinates.Length - 1; i++)
                    {
                        PointF a = CoordToPoint(o.geometry.Coordinates[i]);
                        PointF b = CoordToPoint(o.geometry.Coordinates[i + 1]);

                        if (a.X > 10000 || a.Y > 10000 || b.X > 10000 || b.Y > 10000) continue;
                        if (a.X < -10000 || a.Y < -10000 || b.X < -10000 || b.Y < -10000) continue;

                        if (debug)
                            Console.WriteLine("Drawing point at {0}", a);

                        g.DrawLine(pen, a, b);
                    }
                }
                //g.DrawLine(pen, new PointF(p.Width, p.Height), new PointF());
                //g.DrawLine(pen, new PointF(0, p.Height), new PointF(p.Width, 0));


            }

            using (var pen = new Pen(Color.Green, 1f))
            {
                foreach (var o in allInnerObjects)
                {
                    for (int i = 0; i < o.geometry.Coordinates.Length - 1; i++)
                    {
                        PointF a = CoordToPoint(o.geometry.Coordinates[i]);
                        PointF b = CoordToPoint(o.geometry.Coordinates[i + 1]);

                        if (a.X > 10000 || a.Y > 10000 || b.X > 10000 || b.Y > 10000) continue;
                        if (a.X < -10000 || a.Y < -10000 || b.X < -10000 || b.Y < -10000) continue;

                        if (debug)
                            Console.WriteLine("Drawing circle at {0}", a);
                        int antiScale = (int)(1 / scalingFactor);
                        Rectangle rect = new Rectangle((int)a.X - 10, (int)a.Y - 10, 20, 20);
                        try
                        {
                            g.DrawEllipse(pen, rect);
                        }
                        catch (Exception exc)
                        {
                            Console.WriteLine("Something went wrong while drawing a rectangle.. {0}", exc);
                        }
                    }
                }
            }

            if (currentEdge != null)
            {
                using (var pen = new Pen(Color.Green, 2f))
                {
                    PointF a = CoordToPoint(currentEdge.Coordinates[0]);
                    PointF b = CoordToPoint(currentEdge.Coordinates[1]);

                    if (debug)
                        Console.WriteLine("Drawing circle at {0}", a);
                    int antiScale = (int)(1 / scalingFactor);

                    g.DrawLine(pen, a, b);
                    Rectangle rect = new Rectangle((int)a.X - 10, (int)a.Y - 10, 20, 20);
                    try
                    {
                        g.DrawEllipse(pen, rect);
                    }
                    catch (Exception exc)
                    {
                        Console.WriteLine("Something went wrong while drawing a rectangle.. {0}", exc);
                    }
                    rect = new Rectangle((int)b.X - 10, (int)b.Y - 10, 20, 20);
                    try
                    {
                        g.DrawEllipse(pen, rect);
                    }
                    catch (Exception exc)
                    {
                        Console.WriteLine("Something went wrong while drawing a rectangle.. {0}", exc);
                    }
                }
            }

            if (newVertex != null)
            {
                using (var pen = new Pen(Color.Violet, 1f))
                {
                    PointF a = CoordToPoint(newVertex);

                    if (debug)
                        Console.WriteLine("Drawing circle at {0}", a);
                    int antiScale = (int)(1 / scalingFactor);
                    Rectangle rect = new Rectangle((int)a.X - 6, (int)a.Y - 6, 12, 12);
                    try
                    {
                        g.DrawEllipse(pen, rect);
                    }
                    catch (Exception exc)
                    {
                        Console.WriteLine("Something went wrong while drawing a rectangle.. {0}", exc);
                    }
                }
            }

            int aoisudoaisd = 12;
        }
    }

    public void CenterOnInner()
    {
        Envelope e = new Envelope();

        if (allInnerObjects != null)
        {
            foreach (var o in allInnerObjects)
                e.ExpandToInclude(o.geometry.EnvelopeInternal);
        }

        halfWidth = p.Width / 2;
        halfHeight = p.Height / 2;

        offsetX = (int)e.Centre.X;
        offsetY = (int)e.Centre.Y;

        if (debug)
            Console.WriteLine("Offsets: {0}, {1}", offsetX, offsetY);

        int dataWidth = (int)(e.MaxX - e.MinX);
        int dataHeight = (int)(e.MaxY - e.MinY);

        int maxExtents = Math.Max(dataWidth, dataHeight);

        if (debug)
        {
            Console.WriteLine("Extents w/h: {0}, {1}", dataWidth, dataHeight);
            Console.WriteLine("Picturebox w/h: {0}, {1}", p.Width, p.Height);
        }

        scalingFactor = 0.5f / (maxExtents / (float)Math.Max(p.Width - 50, p.Height - padding * 50));
    }

    public void SetFocus(Geometry g)
    {
        Envelope e = new Envelope();
        e.ExpandToInclude(g.EnvelopeInternal);

        halfWidth = p.Width / 2;
        halfHeight = p.Height / 2;

        offsetX = (int)e.Centre.X;
        offsetY = (int)e.Centre.Y;

        if (debug)
            Console.WriteLine("Offsets: {0}, {1}", offsetX, offsetY);

        int dataWidth = (int)(e.MaxX - e.MinX);
        int dataHeight = (int)(e.MaxY - e.MinY);

        int maxExtents = Math.Max(dataWidth, dataHeight);

        if (debug)
        {
            Console.WriteLine("Extents w/h: {0}, {1}", dataWidth, dataHeight);
            Console.WriteLine("Picturebox w/h: {0}, {1}", p.Width, p.Height);
        }

        scalingFactor = 0.5f / (maxExtents / (float)Math.Max(p.Width - 50, p.Height - padding * 50));
    }

    private PointF CoordToPoint(Coordinate c)
    {
        return new PointF((float)(c.X - offsetX) * scalingFactor + halfWidth, p.Height - (float)(c.Y - offsetY) * scalingFactor - halfHeight);
    }

    public void Reset()
    {

    }
}