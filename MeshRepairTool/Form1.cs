﻿using System;
using System.Windows.Forms;
using System.Drawing;
using NetTopologySuite.Geometries;

namespace MeshRepairTool
{
    public partial class Form1 : Form
    {
        BGTParser bgt;
        PictureBox p;

        public Form1()
        {
            InitializeComponent();
            bgt = new BGTParser();
            p = pictureBox1;
            bgt.pictureBox = pictureBox1;
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            Envelope env = Envelopes.Ahoy;
            bgt.RepairBetweenLayersSimple(env, true, false);
            //bgt.FindConnectionsNew(env);
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            bgt.Paint(e);
        }
    }
}
