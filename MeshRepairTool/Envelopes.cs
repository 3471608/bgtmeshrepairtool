﻿using NetTopologySuite.Geometries;

public static class Envelopes
{
    public static Envelope Utrecht = new Envelope(136000, 137500, 454750, 456750);
    public static Envelope Uithof = new Envelope(139500, 141400, 454700, 455900);
    public static Envelope Amsterdam = new Envelope(120600, 122600, 486000, 488200);
    public static Envelope Amsterdam_Small = new Envelope(121000, 122000, 486500, 487500);
    public static Envelope Nijmegen = new Envelope(186800, 188700, 427800, 429300);
    public static Envelope Ahoy = new Envelope(92600, 93500, 432800, 433800);
    public static Envelope Giethoorn = new Envelope(202000, 203000, 525700, 527500);
    public static Envelope Maastricht = new Envelope(176000, 178000, 317000, 318100);
    public static Envelope Bourtange = new Envelope(275700, 276500, 558700, 559900);
    public static Envelope Utrecht_Small = new Envelope(136500, 137000, 455500, 456000);
    public static Envelope Utrecht_SW = new Envelope(134000, 136000, 454000, 456000);
    public static Envelope Duivendrecht = new Envelope(124000, 125200, 480500, 482200);
    public static Envelope Rotterdam = new Envelope(91000, 93000, 436000, 438000);
    public static Envelope Austerlitz = new Envelope(151400, 152400, 455100, 456100);
}