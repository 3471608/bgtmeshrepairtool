﻿using NetTopologySuite.Geometries;
using System.Numerics;

public class Connection
{
    public SimpleBGTObject a, b;
    public int indexA, indexB;
    public Coordinate coordFirst, coordLast;
    public Geometry geometry;

    public Connection() { }

    public Connection(SimpleBGTObject a, SimpleBGTObject b, int indexA, int indexB, Edge e)
    {
        this.a = a;
        this.b = b;
        this.indexA = indexA;
        this.indexB = indexB;
        coordFirst = e.start;
        coordLast = e.end;
    }
}
