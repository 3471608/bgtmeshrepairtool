﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetTopologySuite.Geometries;

public class SimpleBGTObject
{
    public string ID { get; set; }
    public int relativeHeight { get; set; }
    public string type { get; set; }
    public string surfaceMaterial { get; set; } = string.Empty;
    public string physicalAppearance { get; set; } = string.Empty;
    public string function { get; set; } = string.Empty;
    public string bgtClass { get; set; } = string.Empty;
    public Geometry geometry { get; set; }
    public Polygon polygon { get; set; }
    public MultiPolygon multiPolygon { get; set; }
    public LineString lineString { get; set; }
    public Point point { get; set; }
}
