﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public static class InsertStrings
{
    private const string SCHEMA = "test.";
    private const string INSERT_INTO = "INSERT INTO";
    private const string DEFAULT_ATTRIBUTES = "(gml_id, geometrie, relatievehoogteligging) VALUES(@id, @geometry, @height)" +
        "ON CONFLICT " +
        "DO NOTHING;";
    private const string FYSIEK_VOORKOMEN_BOTH = "(gml_id, geometrie, relatievehoogteligging, bgt_fysiekvoorkomen, plus_fysiekvoorkomen) VALUES(@id, @geometry, @height, @bgt_appearance, @plus_appearance)" +
        "ON CONFLICT " +
        "DO NOTHING;";

    private const string BGT_FYSIEK = "(gml_id, geometrie, relatievehoogteligging, bgt_fysiekvoorkomen) VALUES(@id, @geometry, @height, @bgt_appearance)" +
        "ON CONFLICT " +
        "DO NOTHING;";

    private const string PLUS_TYPE = "(gml_id, geometrie, relatievehoogteligging, plus_type) VALUES(@id, @geometry, @height, @plus_type)" +
        "ON CONFLICT " +
        "DO NOTHING;";

    private const string BGT_FUNCTIE_PLUS_FYSIEK = "(gml_id, geometrie, relatievehoogteligging, bgt_functie, plus_fysiekvoorkomen) VALUES(@id, @geometry, @height, @bgt_function, @plus_appearance)" +
        "ON CONFLICT " +
        "DO NOTHING;";

    private const string BRIDGE_ATTRIBUTES = "(gml_id, geometrie, relatievehoogteligging, typeoverbruggingsdeel, hoortbijtypeoverbrugging) VALUES(@id, @geometry, @height, @bridge_type, @bridge_part_of)" +
        "ON CONFLICT " +
        "DO NOTHING;";


    public static string Pand = INSERT_INTO + " " + SCHEMA + "pand" + " " + DEFAULT_ATTRIBUTES;
    public static string Kunstwerkdeel = INSERT_INTO + " " + SCHEMA + "kunstwerkdeel" + " " + DEFAULT_ATTRIBUTES;
    public static string Ondersteunendwaterdeel = INSERT_INTO + " " + SCHEMA + "ondersteunendwaterdeel" + " " + DEFAULT_ATTRIBUTES;
    public static string Paal = INSERT_INTO + " " + SCHEMA + "paal" + " " + DEFAULT_ATTRIBUTES;
    public static string Scheiding = INSERT_INTO + " " + SCHEMA + "scheiding" + " " + DEFAULT_ATTRIBUTES;
    public static string Tunneldeel = INSERT_INTO + " " + SCHEMA + "tunneldeel" + " " + DEFAULT_ATTRIBUTES;
    public static string Waterdeel = INSERT_INTO + " " + SCHEMA + "waterdeel" + " " + DEFAULT_ATTRIBUTES;

    //public static string Begroeidterreindeel = INSERT_INTO + " " + SCHEMA + "begroeidterreindeel" + " " + FYSIEK_VOORKOMEN_BOTH;
    //public static string Onbegroeidterreindeel = INSERT_INTO + " " + SCHEMA + "onbegroeidterreindeel" + " " + FYSIEK_VOORKOMEN_BOTH;

    //public static string Ondersteunendwegdeel = INSERT_INTO + " " + SCHEMA + "ondersteunendwegdeel" + " " + BGT_FYSIEK;

    //public static string Vegetatieobject = INSERT_INTO + " " + SCHEMA + "vegetatieobject" + " " + PLUS_TYPE;

    //public static string Wegdeel = INSERT_INTO + " " + SCHEMA + "wegdeel" + " " + BGT_FUNCTIE_PLUS_FYSIEK;

    //public static string Overbruggingsdeel = INSERT_INTO + " " + SCHEMA + "overbruggingsdeel" + " " + BRIDGE_ATTRIBUTES;

    public static string Begroeidterreindeel = INSERT_INTO + " " + SCHEMA + "begroeidterreindeel" + " " + DEFAULT_ATTRIBUTES;
    public static string Onbegroeidterreindeel = INSERT_INTO + " " + SCHEMA + "onbegroeidterreindeel" + " " + DEFAULT_ATTRIBUTES;

    public static string Ondersteunendwegdeel = INSERT_INTO + " " + SCHEMA + "ondersteunendwegdeel" + " " + DEFAULT_ATTRIBUTES;

    public static string Vegetatieobject = INSERT_INTO + " " + SCHEMA + "vegetatieobject" + " " + DEFAULT_ATTRIBUTES;

    public static string Wegdeel = INSERT_INTO + " " + SCHEMA + "wegdeel" + " " + DEFAULT_ATTRIBUTES;

    public static string Overbruggingsdeel = INSERT_INTO + " " + SCHEMA + "overbruggingsdeel" + " " + DEFAULT_ATTRIBUTES;

    public static string GetInsertString(string type)
    {
        switch (type)
        {
            case "overbruggingsdeel":
                return Overbruggingsdeel;
            case "wegdeel":
                return Wegdeel;
            case "kunstwerkdeel":
                return Kunstwerkdeel;
            case "ondersteunendwaterdeel":
                return Ondersteunendwaterdeel;
            case "paal":
                return Paal;
            case "pand":
                return Pand;
            case "scheiding":
                return Scheiding;
            case "waterdeel":
                return Waterdeel;
            case "tunneldeel":
                return Tunneldeel;
            case "begroeidterreindeel":
                return Begroeidterreindeel;
            case "onbegroeidterreindeel":
                return Onbegroeidterreindeel;
            case "ondersteunendwegdeel":
                return Ondersteunendwegdeel;
            case "vegetatieobject":
                return Vegetatieobject;

            default: return "";
        }
    }
}